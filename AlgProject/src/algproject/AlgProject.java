package algproject;

import java.io.RandomAccessFile;
import java.util.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author Jereelyf.Cardona
 */
public class AlgProject {

    /**
     * @param args the command line arguments
     */
    static String route = "D:\\Archivos\\";
    static RandomAccessFile fichero = null;
    //Vector where the information will be stored to file
    static Vector<String> arr = new Vector<String>();
    static Vector<String> arrFileName = new Vector<String>();
    static Vector<String> arrVarName = new Vector<String>();

    public static void main(String[] args) {
        // TODO code application logic here    
        long TStart, Tfinish, time; //Variables to determine the execution time
        TStart = System.currentTimeMillis();

        //ALGORITHM
        //String fileComand = JOptionPane.showInputDialog("Enter the name of the command file");
        ReadCommand(route + "commands1" + ".txt");

        Tfinish = System.currentTimeMillis();
        time = Tfinish - TStart;
        System.out.println("Execution time in milliseconds: " + time);

    }

    public static void ReadCommand(String file) {
        //Read the command file
        try (RandomAccessFile r = new RandomAccessFile(file, "rw")) {

            String s = "0";
            while (s != null) {
                s = r.readLine();
                if (s != null) {
                    arr.add(s);
                }
            }
            System.out.println("Successful command reading");
            //Run menu according to the options
            for (int i = 0; i < arr.size(); i++) {
                Menu(arr.elementAt(i));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
        }
    }

    public static void Menu(String sElement) throws IOException {
        String[] part = sElement.split(" ");

        if (part[0].equals("create") && part[2].equals("as")) //Option 1: Create File
        {
            CreateDocument(part[1], part[3]);
        } else if (part[0].equals("assign") && part[2].equals("to")) //Option 2: Assign File
        {
            AssignFile(part[1], part[3]);
        } else if (part[2].equals("sort") && part[4].equals("asc")) //Option 3: Sort File
        {
            SortFile(part[0], part[3]);
        } else if (part[2].equals("rem_doubles")) //Option 4: Delete Duplicates
        {
            DeleteDuplicates(part[0], part[3]);
        } else if (part.length == 3 && part[1].equals("=")) //Option 5: Replace File
        {
            ReplaceFile(part[0], part[2]);
        } else if (part[1].equals("=") && part[3].equals("+")) //Option 6: Concatenate File
        {
            ConcatenateFile(part[0], part[2], part[4]);
        } else {
            System.out.println("The commands were not recognized");
        }
    }

    //Option 1
    public static void CreateDocument(String filename, String varname) {

        filename = route + filename.replaceAll("\"", "");

        arrVarName.add(varname);
        arrFileName.add(filename);

        //Create an empty file
        try (RandomAccessFile r = new RandomAccessFile(filename, "rw")) {
            System.out.println("File was created correctly on the route " + filename + " in the variable " + varname);
        } catch (Exception e) {
           System.out.println("An error was generated in the creation of the file. " + e.toString());
        }
    }

    //Option 2
    public static void AssignFile(String filename, String varname) {

        //Add the storage file to the file name
        filename = route + filename.replaceAll("\"", "");
        
        //Assign variables to file paths
        arrVarName.add(varname);
        arrFileName.add(filename);

        //Create a file with random numbers
        try (RandomAccessFile r = new RandomAccessFile(filename, "rw")) {
            //Arrangement in which the information will be stored
            int[] arr = new int[800000];//800000
            for (int i = 0; i < arr.length; i++) {
                String num = (int) (Math.random() * (900000000 - 100000000)) + 100000000 + "\r\n";
                r.writeBytes(num);
            }
            System.out.println("File was correctly assigned on the route " + filename + " in the variable " + varname);
        } catch (Exception e) {
            System.out.println("An error was generated in the file allocation. " + e.toString());
        }
    }

    //Option 3
    public static void SortFile(String destintvar, String sourcevar) {

        //Check the path of the variables
        for (int i = 0; i < arrVarName.size(); i++) {
            destintvar = (arrVarName.elementAt(i).equals(destintvar)) ? arrFileName.elementAt(i) : destintvar;
            sourcevar = (arrVarName.elementAt(i).equals(sourcevar)) ? arrFileName.elementAt(i) : sourcevar;
        }

        Vector<String> ArrList = new Vector<String>();
        
        try {
            System.out.println("p");
            RandomAccessFile fsource = new RandomAccessFile(sourcevar, "rw");
            String line = null;
            while ((line = fsource.readLine()) != null) {
                ArrList.add(line);
            }
            fsource.close();
            
            //Sort arraylist with the Sort method, which is ordered with the QuickSort method
            Collections.sort(ArrList);

            //A BUFFEREDWRITER-TYPE OBJECT IS CREATED TO BE ABLE TO WRITE INSIDE THE FILE
            BufferedWriter bw = new BufferedWriter(new FileWriter(destintvar));

            System.out.println("The elements of the file were sorted.");
            
            //Save ArrayList in the file
            for (String cadena : ArrList) {
                bw.write(cadena + "\r\n");
            }
            
            bw.close();

        } catch (Exception e) {
            System.out.println("Error al realizar el ordenamiento. " + e.toString());
        }
    }

    //Option 4
    public static void DeleteDuplicates(String destintvar, String sourcevar) {
        //Check the path of the variables
        for (int i = 0; i < arrVarName.size(); i++) {
            destintvar = (arrVarName.elementAt(i).equals(destintvar)) ? arrFileName.elementAt(i) : destintvar;
            sourcevar = (arrVarName.elementAt(i).equals(sourcevar)) ? arrFileName.elementAt(i) : sourcevar;
        }

        Vector<String> ArrList = new Vector<String>();
        try {
            RandomAccessFile fsource = new RandomAccessFile(sourcevar, "rw");
            String line = null;
            while ((line = fsource.readLine()) != null) {
                ArrList.add(line);
            }
            fsource.close();
            
            //HashSet<T> Allows you to add all the elements of the array and returns a collection of objects without duplicates       
            //permite agregar todos los elementos del array y devuelve una colección de objetos sin duplicados   
            HashSet<String> hashSet = new HashSet<String>(ArrList);
            ArrList.clear();
            ArrList.addAll(hashSet);

            //A BUFFEREDWRITER-TYPE OBJECT IS CREATED TO BE ABLE TO WRITE INSIDE THE FILE
            BufferedWriter bw = new BufferedWriter(new FileWriter(destintvar));

            //Save ArrayList in the file
            for (String cadena : ArrList) {
                bw.write(cadena + "\r\n");
            }
            System.out.println("The duplicate records were deleted.");
        
        } catch (Exception e) {
            System.out.println("Error deleting duplicate records. " + e.toString());
        }
    }

    //Option 5
    public static void ReplaceFile(String destintvar, String sourcevar) {
        try {
            //Check the path of the variables
            for (int i = 0; i < arrVarName.size(); i++) {
                destintvar = (arrVarName.elementAt(i).equals(destintvar)) ? arrFileName.elementAt(i) : destintvar;
                sourcevar = (arrVarName.elementAt(i).equals(sourcevar)) ? arrFileName.elementAt(i) : sourcevar;
            }

            FileChannel fcSource = new RandomAccessFile(sourcevar, "r").getChannel();
            FileChannel fcDestint = new RandomAccessFile(destintvar, "rw").getChannel();
            fcDestint.transferFrom(fcSource, 0, Long.MAX_VALUE);
            fcSource.close();
            fcDestint.close();
            System.out.println("Successful Copy");
        } catch (Exception e) {
            System.out.println("An error was generated in the copy of the file." + e.toString() + " \r\n");
        }
    }

    //Option 6
    public static void ConcatenateFile(String destintvar, String sourcevar1, String sourcevar2) {

        long p = 0, dp, sizein, sizeen;
        FileChannel in = null, out = null, en = null;
        try {
            //Check the path of the variables
            for (int i = 0; i < arrVarName.size(); i++) {
                destintvar = (arrVarName.elementAt(i).equals(destintvar)) ? arrFileName.elementAt(i) : destintvar;
                sourcevar1 = (arrVarName.elementAt(i).equals(sourcevar1)) ? arrFileName.elementAt(i) : sourcevar1;
                sourcevar2 = (arrVarName.elementAt(i).equals(sourcevar2)) ? arrFileName.elementAt(i) : sourcevar2;
            }

            FileChannel fcSource1 = new RandomAccessFile(sourcevar1, "r").getChannel();
            FileChannel fcSource2 = new RandomAccessFile(sourcevar2, "r").getChannel();
            FileChannel fcDestint = new RandomAccessFile(destintvar, "rw").getChannel();

            in = new FileInputStream(sourcevar1).getChannel();
            en = new FileInputStream(sourcevar2).getChannel();
            out = new FileOutputStream(destintvar).getChannel();
            sizein = in.size();
            sizeen = en.size();

            while ((dp = out.transferFrom(in, p, sizein)) > 0) {
                p += dp;
            }

            while ((dp = out.transferFrom(en, p, sizeen)) > 0) {
                p += dp;
            }

            System.out.println("Successful Union");
            out.close();
            in.close();
            en.close();

        } catch (Exception e) {
            System.out.println("An error in the union of the files was generated. " + e.toString() + " \r\n");
        }
    }
}
